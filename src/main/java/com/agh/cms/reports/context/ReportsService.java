package com.agh.cms.reports.context;

import com.agh.cms.common.domain.dto.ReportCreateRequest;
import com.agh.cms.common.domain.dto.ReportRow;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.stream.Stream;

@Service
class ReportsService {

    ByteArrayInputStream createReport(ReportCreateRequest request) throws Exception {
        Document document = new Document();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, outputStream);
        document.open();
        document.add(createWorker(request));
        document.add(createReportType(request));
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));
        document.add(createTable(request));
        document.close();
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    private Paragraph createWorker(ReportCreateRequest request) {
        Font font = FontFactory.getFont(FontFactory.TIMES, 20, BaseColor.BLACK);
        Chunk chunk = new Chunk("Username: " + request.worker + "\n", font);
        return new Paragraph(chunk);
    }

    private Paragraph createReportType(ReportCreateRequest request) {
        Font font = FontFactory.getFont(FontFactory.TIMES, 20, BaseColor.BLACK);
        Chunk chunk = new Chunk("Report type: " + request.reportType.getName() + "\n", font);
        return new Paragraph(chunk);
    }

    private PdfPTable createTable(ReportCreateRequest request) {
        PdfPTable table = new PdfPTable(2);
        addTableHeader(table);
        addRows(table, request);
        return table;
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("From", "To")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table, ReportCreateRequest request) {
        for (ReportRow row : request.reportRows) {
            table.addCell(row.from);
            table.addCell(row.to);
        }
    }
}
