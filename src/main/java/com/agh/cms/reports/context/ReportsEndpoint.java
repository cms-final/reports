package com.agh.cms.reports.context;

import com.agh.cms.common.domain.dto.ReportCreateRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;

@CrossOrigin
@RestController
@RequestMapping("/reports")
class ReportsEndpoint {

    private final ReportsService reportsService;

    ReportsEndpoint(ReportsService reportsService) {
        this.reportsService = reportsService;
    }

    @PostMapping
    ResponseEntity<byte[]> createReport(@Valid @RequestBody ReportCreateRequest request) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=report.pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        ByteArrayInputStream report = reportsService.createReport(request);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(report.readAllBytes());
    }
}
